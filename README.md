# Mini Project 1

This mini project app displays some issues from React Facebook repository. It display info of each issue also the participants commented to the issues.

## Data Requirements

### Issue Info

- name
- owner

### Issue

- number of issue (OPEN and CLOSED)
- id
- title
- ticket number
- author
- createdAt
- number of comments each issue

## Issue Details & Comments

- comment author
- comment avatar url
- comment body
- comment createdAt

## Issue Author

- login
- name
- location
- followers
- following
- avatar url

## GraphQL Schema

```graphql
type Query {
  repository: Repository
}

type Repository {
  name: String
  createdAt: String
  owner: Author
  issues: Issue
}

type Author {
  login: String
  avatarUrl: String
}

type Issue {
  totalCount: Int
  nodes: [Nodes]
}

type Nodes {
  id: ID
  number: Int
  title: String
  author: Author
  createdAt: String
  url: String
  comments: Comment
}

type Comment {
  totalCount: Int
  nodes: [NodeComment]
}

type NodeComment {
  author: Author
  body: String
  createdAt: String
}
```

## GraphQL Query

These query is used by the server to fetch some data that will be passed to the clients.

```graphql
query {
  repository(owner: "facebook", name: "react") {
    name
    owner {
      login
      avatarUrl
    }
    createdAt
    issues(
      first: 10
      orderBy: { field: CREATED_AT, direction: DESC }
      states: OPEN
    ) {
      totalCount
      nodes {
        id
        createdAt
        title
        number
        author {
          login
          avatarUrl
        }
        url
        comments(last: 10) {
          totalCount
          nodes {
            author {
              login
              avatarUrl
            }
            body
            createdAt
          }
        }
      }
    }
  }
}
```

## GraphQL Client

This how we fetch the data from GraphQL server.

```javascript
const query = `
  query {
    repository {
      name
      owner { login, avatarUrl }
      createdAt
      issues {
        totalCount
        nodes {
          id
          createdAt
          number
          title
          author { login, avatarUrl }
          url
          comments {
            totalCount
            nodes {
              author { login, avatarUrl }
              body
              createdAt
            }
          }
        }
      }
    }
  }
`
```
