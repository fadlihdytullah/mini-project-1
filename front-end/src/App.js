import React, { Component } from 'react'
import Header from './components/layouts/Header'
import Issues from './components/Issues'
import IssueDetails from './components/IssueDetails'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'

import './App.css'
import axios from 'axios'

class App extends Component {
  constructor() {
    super()
    this.state = {
      repository: {
        name: '',
        createdAt: '',
        owner: {
          login: '',
          avatarUrl: ''
        },
        issues: {
          totalCount: 0,
          nodes: []
        }
      }
    }
  }

  async componentDidMount() {
    const endpoint = 'http://localhost:8080/graphql'

    const query = `
      query {
        repository {
          name
          owner { login, avatarUrl }
          createdAt
          issues {
            totalCount
            nodes {
              id
              createdAt
              number
              title
              author { login, avatarUrl }
              url
              comments {
                totalCount
                nodes {
                  author { login, avatarUrl }
                  body
                  createdAt
                }
              }
            }
          }
        }
      }
    `

    const opt = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      },
      data: { query }
    }

    const res = await axios(endpoint, opt)
    const repository = res.data.data.repository

    console.log(repository.issues.nodes)

    this.setState({
      repository
    })
  }

  render() {
    const {
      name,
      createdAt,
      owner: { login, avatarUrl },
      issues: { totalCount }
    } = this.state.repository

    const { issues } = this.state.repository

    const repoInfo = { name, createdAt, login, avatarUrl, totalCount }

    return (
      <div className='App'>
        <Header
          title='IssueHub'
          description='View Github Issue Easier'
          repoInfo={repoInfo}
        />

        <Router>
          <Route exact path='/' component={() => <Issues issues={issues} />} />
          <Route path='/issue/:number' component={IssueDetails} />
        </Router>
      </div>
    )
  }
}

export default App
