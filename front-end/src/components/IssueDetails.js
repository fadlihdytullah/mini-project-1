import React, { Component } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'

class IssueDetails extends Component {
  constructor() {
    super()
    this.state = {
      node: {
        title: '',
        number: '',
        author: {
          login: '',
          avatarUrl: ''
        }
      }
    }
  }

  componentDidMount() {
    if (this.props.location.query) {
      const { node } = this.props.location.query
      this.setState({ node })
      console.log(node)
    } else this.props.history.push('/')
  }

  render() {
    const { title, number, author, createdAt, comments } = this.state.node

    let commentElements = null
    if (comments) {
      commentElements = comments.nodes.map(comment => (
        <div className='comment-item mb-1 p-2 rounded shadow'>
          <div className='d-flex'>
            <div className='mr-2 d-flex flex-center'>
              <div className='text-center'>
                <img
                  className='-comment-image'
                  style={{ width: 50, display: 'block' }}
                  src={comment.author.avatarUrl}
                />
                <span className='text-muted text-bold'>
                  {comment.author.login}
                </span>
              </div>
            </div>
            <div>
              <span className='text-muted'>{comment.createdAt}</span>
              <div>{comment.body}</div>
            </div>
          </div>
        </div>
      ))
    }

    const newDate = new Date(createdAt)
    const year = newDate.getFullYear()
    const month = newDate.getMonth()
    const monthList = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'Augustus',
      'September',
      'October',
      'November',
      'December'
    ]
    const date = newDate.getDate()

    return (
      <div className='container'>
        <div className='issue' style={{ padding: '3rem' }}>
          <div className='-issue-info d-flex' style={{ marginBottom: '2rem' }}>
            <div className='d-flex flex-center mr-2'>
              <div className='text-center'>
                <img
                  className='rounded'
                  style={{ width: 75, display: 'block' }}
                  src={author.avatarUrl}
                />
                <span className='text-muted text-bold'>{author.login}</span>
              </div>
            </div>

            <div>
              <span className='text-muted'>{`${
                monthList[month]
              } ${date}, ${year}`}</span>
              <h2>
                {title} &mdash; <span className='text-muted'>#{number}</span>
              </h2>
            </div>
          </div>
          {commentElements}
        </div>
      </div>
    )
  }
}

export default IssueDetails
