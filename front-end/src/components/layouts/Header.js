import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Header extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    repoInfo: PropTypes.object.isRequired
  }

  render() {
    const { title, description } = this.props

    const {
      name,
      createdAt,
      login,
      avatarUrl,
      totalCount
    } = this.props.repoInfo

    return (
      <header>
        <nav>
          <h1>
            {title} <span className='text-muted'>&mdash; {description}</span>
          </h1>
        </nav>
        <div className='d-flex flex-center py-4'>
          <div className='repo-info d-flex'>
            <div className='-repo-image mr-2 d-flex flex-center'>
              <img src={avatarUrl} alt='Github user' />
            </div>
            <div className='-repo-details'>
              <div className='--publish-date text-muted'>{createdAt}</div>
              <h1 className='--repo-name'>
                {name} <span>&mdash; {login}</span>
              </h1>
              <div>
                Total <span className='--open-state'>Open</span> Issues:{' '}
                <span className='text-bold text-bigger'>{totalCount}</span>
              </div>
            </div>
          </div>
        </div>
      </header>
    )
  }
}

export default Header
