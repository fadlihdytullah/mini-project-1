import React, { Component } from 'react'
import IssueItem from './IssueItem'
import { Link } from 'react-router-dom'

class Issues extends Component {
  render() {
    const { nodes } = this.props.issues
    const issuesComponent = nodes.map(node => (
      <Link
        key={node.id}
        to={{ pathname: `/issue/${node.number}`, query: { node } }}
      >
        <IssueItem className='issue-item' node={node} />
      </Link>
    ))

    return (
      <div className='container'>
        <div className='issue-list'>
          <h2 className='text-muted my-2'>Issue Lists</h2>

          {issuesComponent}
        </div>
      </div>
    )
  }
}

export default Issues
