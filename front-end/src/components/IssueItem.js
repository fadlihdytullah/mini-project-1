import React from 'react'

function IssueItem({ node }) {
  const {
    createdAt,
    title,
    number,
    author: { login },
    comments: { totalCount }
  } = node

  const newDate = new Date(createdAt)
  const year = newDate.getFullYear()
  const month = newDate.getMonth()
  const monthList = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'Augustus',
    'September',
    'October',
    'November',
    'December'
  ]
  const date = newDate.getDate()

  return (
    <div className='-issue-item d-flex p-2 shadow rounded mb-1'>
      <div className='--issue-content-box'>
        <div className='--issue-date text-muted'>{`${
          monthList[month]
        } ${date}, ${year}`}</div>
        <h3 className='--issue-title'>{title}</h3>
        <div className='--issue-author'>
          <span className='text-muted'>#{number} opened by</span>{' '}
          <span>{login}</span>
        </div>
      </div>

      <div className='--issue-comment-box'>
        <div>
          {totalCount} <ion-icon name='ios-chatboxes' />
        </div>
      </div>
    </div>
  )
}

export default IssueItem
