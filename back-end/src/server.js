const express = require('express')
const express_graphql = require('express-graphql')
const { buildSchema } = require('graphql')
const path = require('path')
const fs = require('fs')
const cors = require('cors')
const axios = require('axios')
const dotenv = require('dotenv')
dotenv.config()

const endpoint = 'https://api.github.com/graphql'

// GraphQL Schema
const schema = buildSchema(`
  type Query {
    repository: Repository
  }

  type Repository {
    name: String
    createdAt: String
    owner: Author
    issues: Issue
  }

  type Author {
    login: String
    avatarUrl: String
  }

  type Issue {
    totalCount: Int
    nodes: [Nodes]
  }

  type Nodes {
    id: ID
    number: Int
    title: String
    author: Author
    createdAt: String
    url: String
    comments: Comment
  }

  type Comment {
    totalCount: Int
    nodes: [NodeComment]
  }

  type NodeComment {
    author: Author
    body: String
    createdAt: String
  }
`)

const opt = {
  url: endpoint,
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-type': 'application/json',
    Authorization: `bearer be444aa0eb22966adddae0cea6ffaf8dabcfa727`,
    'Access-Control-Allow-Origin': '*'
  }
}

const getUser = async () => {
  const query = `
    query {
      user(login: "fadlihdytullah") {
        name
        location
        bio
        url
        avatarUrl
      }
    }
  `

  const res = await axios({
    ...opt,
    data: { query }
  })
  const user = res.data.data.user
  return user
}

const getRepository = async () => {
  const query = `
    query {
      repository(owner:"facebook", name: "react") {
        name
        owner { login, avatarUrl }
        createdAt
        issues(first: 10, orderBy: { field: CREATED_AT, direction: DESC }, states: OPEN) {
          totalCount
          nodes 
          {
            id
            createdAt
            title
            number
            author { login, avatarUrl }
            url
            comments(last: 10) 
            {
              totalCount
              nodes 
              {
                author { login, avatarUrl }
                body
                createdAt
              }
            }
          }
        }
      }
    }
  `

  const res = await axios({
    ...opt,
    data: { query }
  })
  const repository = res.data.data.repository

  console.log(repository)
  fs.writeFile('data.json', JSON.stringify(repository), err => {
    if (err) throw err
    console.log('Saved')
  })

  return repository
}

const root = {
  user: getUser,
  repository: getRepository
}

const app = express()

app.use(
  cors({
    origin: true,
    credentials: true
  })
)

app.use(
  '/graphql',
  express_graphql({
    schema,
    rootValue: root,
    graphiql: true
  })
)
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, './index.html'))
})

app.listen(8080, () =>
  console.log('The GraphQL Server has Running on localhost:8080/graphql')
)
