"use strict";

var _express = _interopRequireDefault(require("express"));

var _expressGraphql = _interopRequireDefault(require("express-graphql"));

var _graphql = require("graphql");

var _path = _interopRequireDefault(require("path"));

var _fs = _interopRequireDefault(require("fs"));

var _cors = _interopRequireDefault(require("cors"));

var _axios = _interopRequireDefault(require("axios"));

var _dotenv = _interopRequireDefault(require("dotenv"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { if (i % 2) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } else { Object.defineProperties(target, Object.getOwnPropertyDescriptors(arguments[i])); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

_dotenv["default"].config();

var endpoint = 'https://api.github.com/graphql'; // GraphQL Schema

var schema = (0, _graphql.buildSchema)("\n  type Query {\n    repository: Repository\n  }\n\n  type Repository {\n    name: String\n    createdAt: String\n    owner: Author\n    issues: Issue\n  }\n\n  type Author {\n    login: String\n    avatarUrl: String\n  }\n\n  type Issue {\n    totalCount: Int\n    nodes: [Nodes]\n  }\n\n  type Nodes {\n    id: ID\n    number: Int\n    title: String\n    author: Author\n    createdAt: String\n    url: String\n    comments: Comment\n  }\n\n  type Comment {\n    totalCount: Int\n    nodes: [NodeComment]\n  }\n\n  type NodeComment {\n    author: Author\n    body: String\n    createdAt: String\n  }\n");
var opt = {
  url: endpoint,
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-type': 'application/json',
    Authorization: "bearer be444aa0eb22966adddae0cea6ffaf8dabcfa727",
    'Access-Control-Allow-Origin': '*'
  }
};

var getUser =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var query, res, user;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            query = "\n    query {\n      user(login: \"fadlihdytullah\") {\n        name\n        location\n        bio\n        url\n        avatarUrl\n      }\n    }\n  ";
            _context.next = 3;
            return (0, _axios["default"])(_objectSpread({}, opt, {
              data: {
                query: query
              }
            }));

          case 3:
            res = _context.sent;
            user = res.data.data.user;
            return _context.abrupt("return", user);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function getUser() {
    return _ref.apply(this, arguments);
  };
}();

var getRepository =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2() {
    var query, res, repository;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            query = "\n    query {\n      repository(owner:\"facebook\", name: \"react\") {\n        name\n        owner { login, avatarUrl }\n        createdAt\n        issues(first: 10, orderBy: { field: CREATED_AT, direction: DESC }, states: OPEN) {\n          totalCount\n          nodes \n          {\n            id\n            createdAt\n            title\n            number\n            author { login, avatarUrl }\n            url\n            comments(last: 10) \n            {\n              totalCount\n              nodes \n              {\n                author { login, avatarUrl }\n                body\n                createdAt\n              }\n            }\n          }\n        }\n      }\n    }\n  ";
            _context2.next = 3;
            return (0, _axios["default"])(_objectSpread({}, opt, {
              data: {
                query: query
              }
            }));

          case 3:
            res = _context2.sent;
            repository = res.data.data.repository;
            console.log(repository);

            _fs["default"].writeFile('data.json', JSON.stringify(repository), function (err) {
              if (err) throw err;
              console.log('Saved');
            });

            return _context2.abrupt("return", repository);

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function getRepository() {
    return _ref2.apply(this, arguments);
  };
}();

var root = {
  user: getUser,
  repository: getRepository
};
var app = (0, _express["default"])();
app.use((0, _cors["default"])({
  origin: true,
  credentials: true
}));
app.use('/graphql', (0, _expressGraphql["default"])({
  schema: schema,
  rootValue: root,
  graphiql: true
}));
app.get('/', function (req, res) {
  res.sendFile(_path["default"].join(__dirname, './index.html'));
});
app.listen(8080, function () {
  return console.log('The GraphQL Server has Running on localhost:8080/graphql');
});